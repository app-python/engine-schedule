
module "SCHEDULE-NODE-A" {
  source                      = "git::gitrepos"
  env                         = var.env
  scheduled_action_name_start = var.scheduled_action_name_start
  scheduled_action_name_stop  = var.scheduled_action_name_stop
  recurrence_start            = var.recurrence_start
  recurrence_stop             = var.recurrence_stop
  autoscaling_group_name      = module.eks.NAME-AUTOSCALING[0]
}

module "SCHEDULE-NODE-B" {
  source                      = "git::gitrepos"
  env                         = var.env
  scheduled_action_name_start = var.scheduled_action_name_start
  scheduled_action_name_stop  = var.scheduled_action_name_stop
  recurrence_start            = var.recurrence_start
  recurrence_stop             = var.recurrence_stop
  autoscaling_group_name      = module.eks.NAME-AUTOSCALING[1]
}